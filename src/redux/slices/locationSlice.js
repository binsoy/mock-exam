import { createSlice } from '@reduxjs/toolkit'

const locationsSlice = createSlice({
  name: 'locations',
  initialState: {
    locations: [],
    location: { id: '', location: '', description: '' },
  },
  reducers: {
    addLocation: {
      reducer(state, action) {
        state.locations.push({ ...action.payload })
      },
      // Won't work :(
      prepare({ location, description }) {
        const randomId = `${Math.floor(Math.random() * 500) + 400}`
        return {
          payload: {
            id: randomId,
            location,
            description,
          },
        }
      },
    },
    getLocations(state, action) {
      state.locations = action.payload
    },
    getLocation(state, action) {
      state.location = state.locations.find((l) => l.id === action.payload)
    },
    updateLocation(state, action) {
      const location = state.locations.find((l) => l.id === action.payload.id)
      location.location = action.payload.location
      location.description = action.payload.description
    },
    removeLocation(state, action) {
      state.locations = state.locations.filter((l) => l.id !== action.payload)
    },
    resetLocation(state) {
      state.location = { id: '', location: '', description: '' }
    },
    resetLocations(state) {
      state.locations = []
    },
  },
})

export const {
  addLocation,
  updateLocation,
  getLocations,
  getLocation,
  removeLocation,
  resetLocation,
  resetLocations,
} = locationsSlice.actions

export default locationsSlice.reducer
