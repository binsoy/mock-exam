import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit'
import createSagaMiddleware from 'redux-saga'
import locationReducer from '../redux/slices/locationSlice'
import rootSaga from '../redux/sagas/rootSaga'

const sagaMiddleware = createSagaMiddleware()
const middleware = [...getDefaultMiddleware({ thunk: false }), sagaMiddleware]

const store = configureStore({
  reducer: locationReducer,
  middleware,
})

sagaMiddleware.run(rootSaga)

export default store
