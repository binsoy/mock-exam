import { call, put, takeLatest } from 'redux-saga/effects'
import {
  getLocations,
  addLocation,
  getLocation,
  updateLocation,
  removeLocation,
  resetLocation,
  resetLocations,
} from '../slices/locationSlice'
import { locationActions } from './sagaActions'

const apiUrl = 'https://5f3430949124200016e18826.mockapi.io/api/locations'

function getLocationsFromApi() {
  return fetch(apiUrl, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then((res) => res.json())
    .catch((err) => {
      throw err
    })
}

function* fetchLocations() {
  try {
    const locations = yield call(getLocationsFromApi)
    yield put({ type: getLocations.toString(), payload: locations })
  } catch (e) {
    yield put({
      type: resetLocations.toString(),
      payload: { message: e.message },
    })
  }
}

function* addLocationToApi(action) {
  yield put({ type: addLocation.toString(), payload: action.payload })
}

function* updateLocationDetails(action) {
  yield put({ type: updateLocation.toString(), payload: action.payload })
}

function* deleteLocation(action) {
  yield put({ type: removeLocation.toString(), payload: action.payload })
}

function* getLocationFromApi(action) {
  yield put({ type: getLocation.toString(), payload: action.payload })
}

function* resetLocationInState() {
  yield put({ type: resetLocation.toString() })
}

function* locationSaga() {
  yield takeLatest(locationActions.FETCH_LOCATIONS, fetchLocations)
  yield takeLatest(locationActions.ADD_LOCATION, addLocationToApi)
  yield takeLatest(locationActions.GET_LOCATION, getLocationFromApi)
  yield takeLatest(locationActions.RESET_LOCATION, resetLocationInState)
  yield takeLatest(locationActions.UPDATE_LOCATION, updateLocationDetails)
  yield takeLatest(locationActions.DELETE_LOCATION, deleteLocation)
}

export default locationSaga
