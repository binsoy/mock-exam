import { useEffect, useState } from 'react'
import './App.css'
import logo from './logo.svg'

//Redux
import { useDispatch, useSelector } from 'react-redux'
import { locationActions } from './redux/sagas/sagaActions'

//Material UI
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

//Components
import LocationTable from './components/LocationTable'
import FormDialog from './components/FormDialog'

function App() {
  const dispatch = useDispatch()
  const [open, setOpen] = useState(false)
  const [isEdit, setIsEdit] = useState(false)

  const locations = useSelector((state) => state.locations)

  useEffect(() => {
    dispatch({ type: locationActions.FETCH_LOCATIONS })
  }, [dispatch])

  const handleOpenDialog = (e, locationId) => {
    if (locationId) {
      // Populate dialog
      dispatch({ type: locationActions.GET_LOCATION, payload: locationId })
      setIsEdit(true)
    } else {
      setIsEdit(false)
    }

    setOpen(true)
  }

  const handleCloseDialog = () => {
    setOpen(false)
    dispatch({ type: locationActions.RESET_LOCATION })
  }

  const handleFormSubmit = (data) => {
    if (isEdit) {
      dispatch({ type: locationActions.UPDATE_LOCATION, payload: data })
    } else {
      // Had to do it here since the prepare function in slice won't work :(
      data.id = `${Math.floor(Math.random() * 500) + 400}`
      dispatch({ type: locationActions.ADD_LOCATION, payload: data })
    }
  }

  return (
    <Grid container spacing={2} style={{ marginTop: 100 }}>
      <Grid item sm={2} xs={12} />
      <Grid item sm={8} xs={12}>
        <Typography variant='h2' gutterBottom align='center'>
          MOCK EXAM
        </Typography>
        {locations && locations.length > 0 ? (
          <>
            <Button
              variant='contained'
              color='primary'
              onClick={handleOpenDialog}
              style={{ marginBottom: 15 }}
            >
              +
            </Button>
            <FormDialog
              open={open}
              handleCloseDialog={handleCloseDialog}
              handleFormSubmit={handleFormSubmit}
            />
            <LocationTable
              locations={locations}
              handleOpenDialog={handleOpenDialog}
            />
          </>
        ) : (
          <img src={logo} className='App-logo' alt='logo' />
        )}
      </Grid>
      <Grid item sm={2} xs={12}></Grid>
    </Grid>
  )
}

export default App
