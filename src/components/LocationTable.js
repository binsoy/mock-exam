import { useState } from 'react'

//Material UI
import { makeStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TablePagination from '@material-ui/core/TablePagination'
import TableRow from '@material-ui/core/TableRow'
import Button from '@material-ui/core/Button'

//Redux
import { useDispatch } from 'react-redux'
import { locationActions } from '../redux/sagas/sagaActions'

const columns = [
  { id: 'id', label: 'Id', minWidth: 50 },
  { id: 'location', label: 'Location', minWidth: 100 },
  {
    id: 'description',
    label: 'Description',
    minWidth: 170,
  },
  {
    id: 'actionButtons',
    label: 'Actions',
    minWidth: 100,
    textAlign: 'center',
  },
]

const useStyles = makeStyles({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 500,
  },
})

const LocationTable = ({ locations, handleOpenDialog }) => {
  const dispatch = useDispatch()
  const classes = useStyles()
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(10)

  const handleChangePage = (event, newPage) => {
    setPage(newPage)
  }

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value)
    setPage(0)
  }

  const handleDelete = (e, id) => {
    if (window.confirm('Delete this location?')) {
      dispatch({ type: locationActions.DELETE_LOCATION, payload: id })
    }
  }

  return (
    <Paper className={classes.root}>
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label='sticky table'>
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{
                    minWidth: column.minWidth,
                    textAlign: column.textAlign,
                  }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {locations &&
              locations
                .concat()
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((location) => {
                  return (
                    <TableRow
                      hover
                      role='checkbox'
                      tabIndex={-1}
                      key={location.id}
                    >
                      {columns.map((column) => {
                        const value = location[column.id]
                        return (
                          <TableCell key={column.id} align={column.align}>
                            {column.id !== 'actionButtons' ? (
                              value
                            ) : (
                              <div style={{ textAlign: 'center' }}>
                                <Button
                                  color='primary'
                                  onClick={(e) =>
                                    handleOpenDialog(e, location.id)
                                  }
                                >
                                  Edit
                                </Button>
                                <Button
                                  color='secondary'
                                  onClick={(e) => handleDelete(e, location.id)}
                                >
                                  Delete
                                </Button>
                              </div>
                            )}
                          </TableCell>
                        )
                      })}
                    </TableRow>
                  )
                })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component='div'
        count={locations && (locations.length > 0 ? locations.length : 0)}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </Paper>
  )
}

export default LocationTable
