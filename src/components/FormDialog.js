import { useForm } from 'react-hook-form'

//Material UI
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'

//Redux
import { useSelector } from 'react-redux'

const FormDialog = ({ open, handleCloseDialog, handleFormSubmit }) => {
  const { id, location, description } = useSelector((state) => state.location)

  const {
    register,
    handleSubmit,
    errors,
    formState: { isSubmitting },
  } = useForm()

  const onSubmit = (data) => {
    handleCloseDialog()
    data.id = id
    handleFormSubmit(data)
  }

  return (
    <Dialog
      open={open}
      onClose={() => {
        handleCloseDialog()
      }}
      aria-labelledby='form-dialog-title'
    >
      <DialogTitle id='form-dialog-title'>
        {id ? 'Edit' : 'Add'} Location
      </DialogTitle>
      <form className='App' onSubmit={handleSubmit(onSubmit)}>
        <DialogContent>
          <DialogContentText>
            Please input the required fields to {id ? 'edit' : 'add'} a
            location.
          </DialogContentText>
          <TextField
            autoFocus
            margin='dense'
            id='location'
            label='Location'
            type='text'
            fullWidth
            name='location'
            defaultValue={location}
            inputRef={register({ required: true })}
          />
          {errors.location && errors.location.type === 'required' && (
            <p style={{ color: 'red', textAlign: 'left' }}>
              This field is required
            </p>
          )}

          <TextField
            multiline
            margin='dense'
            id='description'
            label='Description'
            type='text'
            fullWidth
            name='description'
            defaultValue={description}
            inputRef={register({ required: true })}
          />
          {errors.description && errors.description.type === 'required' && (
            <p style={{ color: 'red', textAlign: 'left' }}>
              This field is required
            </p>
          )}
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => {
              handleCloseDialog()
            }}
            color='primary'
          >
            Cancel
          </Button>
          <Button color='primary' type='submit' disabled={isSubmitting}>
            Save
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  )
}

export default FormDialog
